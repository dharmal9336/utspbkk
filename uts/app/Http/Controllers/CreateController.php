<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class CreateController extends Controller
{
    public function index(){
        return view('create021190039');
    }


    public function proses(Request $request){
            $data = array();
            $data ['NPM'] = $request->NPM;
            $data['nama'] = $request->nama;
            $data['programstudi'] = $request->programstudi;
            $data['NOHP'] = $request->NOHP;
            $data['TTL'] = $request->TTL;
            $data['jeniskelamin'] = $request->jeniskelamin;
            $data['Agama'] = $request->Agama;

            return view('view021190039', ['data' => $data]);
        }

    }